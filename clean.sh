#!/bin/bash
find . -name "*.pyc" -type f -delete
find . -name "*.o" -type f -delete
find . -name "aux.*" -type f -delete
find . -name "*.aux" -type f -delete
find . -name "*.bak" -type f -delete
find . -name "*.class" -type f -delete
