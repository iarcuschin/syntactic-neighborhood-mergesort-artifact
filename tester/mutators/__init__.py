import os
from utils.module_utils import *

current_dir = os.path.dirname(os.path.abspath(__file__))
mutators = import_plugins(current_dir, create_instance=True)
