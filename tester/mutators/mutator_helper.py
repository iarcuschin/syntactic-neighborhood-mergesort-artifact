def is_mutable_line(line):
	# Common
	if not line:
		return False
	# C
	if line.startswith("#") or line.startswith("assert"):
		return False
	# Charity
	if line.startswith("rf"):
		return False
	# Java
	if line.startswith("package") or line.startswith("import"):
		return False
	return True
