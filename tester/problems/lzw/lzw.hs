import Data.List
import Data.Char
import Data.Maybe
import Control.Monad
import Control.Arrow
import System.Environment

take2 = filter((==2).length). map (take 2). tails

doLZW _ [] = []
doLZW as (x:xs) =  lzw (map return as) [x] xs
   where lzw a w [] = [fromJust $ elemIndex w a]
         lzw a w (x:xs)  | w' `elem` a = lzw a w' xs
                         | otherwise   = fromJust (elemIndex w a) : lzw (a++[w']) [x] xs
              where w' = w++[x]

main = do
    args <- getArgs
    let str = head args
    putStrLn (doLZW ['\0'..'\255'] str)
