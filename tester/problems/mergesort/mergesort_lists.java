import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

public class mergesort_lists {
    public static void main(String[] args) {
        int n = args.length;
        List<Integer> input = new ArrayList<Integer>();
        for (int i = 0; i < n; i++) {
            input.add(Integer.valueOf(args[i]));
        }
        List<Integer> sorted = mergeSort(input);
        for (int i = 0; i < n - 1; i++) {
            System.out.print(sorted.get(i));
            System.out.print(" ");
        }
        System.out.print(sorted.get(n - 1));
    }
    public static List<Integer> mergeSort(List<Integer> m) {
        if (m.size() <= 1) return m;

        List<Integer> left = m.subList(0, m.size() / 2);
        List<Integer> right = m.subList(m.size() / 2, m.size());

        right = mergeSort(right);
        left = mergeSort(left);

        return merge(left, right);
    }

    public static List<Integer> merge(List<Integer> left, List<Integer> right) {
        List<Integer> result = new ArrayList<Integer>();
        Iterator<Integer> it1 = left.iterator();
        Iterator<Integer> it2 = right.iterator();

        Integer x = it1.next();
        Integer y = it2.next();
        while (true) {
            if (x <= y) {
                result.add(x);
                if (it1.hasNext()) {
                    x = it1.next();
                } else {
                    result.add(y);
                    while (it2.hasNext()) {
                        result.add(it2.next());
                    }
                    break;
                }
            } else {
                result.add(y);
                if (it2.hasNext()) {
                    y = it2.next();
                } else {
                    result.add(x);
                    while (it1.hasNext()) {
                        result.add(it1.next());
                    }
                    break;
                }
            }
        }
        return result;
    }
}
