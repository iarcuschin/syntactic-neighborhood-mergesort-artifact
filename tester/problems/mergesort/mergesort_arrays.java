public class mergesort_arrays {

    public static void main(String[] args) {
        int n = args.length;
        int[] input = new int[n];
        for (int i = 0; i < n; i++) {
            input[i] = Integer.valueOf(args[i]);
        }
        int[] sorted = mergeSort(input);
        for (int i = 0; i < n - 1; i++) {
            System.out.print(sorted[i]);
            System.out.print(" ");
        }
        System.out.print(sorted[n - 1]);
    }

    public static int[] mergeSort(int[] m) {
        if (m.length <= 1) return m;

        int[] left = new int[m.length / 2];
        int[] right = new int[m.length - m.length / 2];
        for (int i = 0; i < m.length; i++) {
            if (i < m.length / 2) {
                left[i] = m[i];
            } else {
                right[i - m.length/2] = m[i];
            }
        }

        right = mergeSort(right);
        left = mergeSort(left);

        return merge(left, right);
    }

    public static int[] merge(int[] left, int[] right) {
        int[] result = new int[left.length + right.length];
        int left_pos = 0;
        int right_pos = 0;

        while(true) {
            if (left_pos < left.length && right_pos < right.length) {
                if (left[left_pos] < right[right_pos]) {
                    result[left_pos + right_pos] = left[left_pos];
                    left_pos++;
                } else {
                    result[left_pos + right_pos] = right[right_pos];
                    right_pos++;
                }
            } else if (left_pos < left.length) {
                result[left_pos + right_pos] = left[left_pos];
                left_pos++;
            } else if (right_pos < right.length) {
                result[left_pos + right_pos] = right[right_pos];
                right_pos++;
            } else {
                break;
            }
        }

        return result;
    }
}
