#include <stdio.h>

void quick_sort (int *a, int n) {
    int i, j, p, t;
    if (n < 2)
        return;
    p = a[n / 2];
    for (i = 0, j = n - 1;; i++, j--) {
        while (a[i] < p)
            i++;
        while (p < a[j])
            j--;
        if (i >= j)
            break;
        t = a[i];
        a[i] = a[j];
        a[j] = t;
    }
    quick_sort(a, i);
    quick_sort(a + i, n - i);
}

int main (int argc, char *argv[]) {
    int i;
    int array[argc-1];
    for(i = 1; i <= argc-1; i++) {
        array[i-1] = atoi(argv[i]);
    }

    int n = sizeof array / sizeof array[0];
    quick_sort(array, n);

    for (i = 0; i <= n-1; ++i) {
        printf("%d%s", array[i], i == n-1 ? "" : " ");
    }
    return 0;
}
