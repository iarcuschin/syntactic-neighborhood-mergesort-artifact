from utils.file_utils import *
from language_helpers import *
from utils.command import TimerTask

import sys
from numpy import random
from numpy import array

class ProblemBase(object):

	def __init__(self):
		# test parameters
		self.test_suite_size = 20
		self.test_size = 10
		# Error logs
		self.verbose = False
		self.logging = True

		self.latex_output = False

		self.programs = [self.base_dir + p for p in get_file_names(self.base_dir) if has_known_extension(p)]
		if self.verbose: print '{0} programs: \n\t{1}'.format(self.get_full_name(), self.programs)
		self.programs_filter = lambda p: True

		self.mutators = []

	def validate_programs(self):
		self.genereate_tests_if_needed()
		filtered_programs = [p for p in self.programs if self.programs_filter(p)]

		for p in filtered_programs:
			print 'Validating program ' + p
			helper = get_language_helper(p, self.base_dir)
			helper.tests = self.get_tests_for_program(p)
			helper.verbose = self.verbose
			ok = helper.validate()
			if ok:
				if self.verbose: print 'Program ' + p + ' correctly validated'
			else:
				print 'Program ' + p + ' failed validation.'
				sys.exit(1)

	def measure_time(self, count):
		self.generate_tests(count=count)
		filtered_programs = [p for p in self.programs if self.programs_filter(p)]

		for p in filtered_programs:
			print 'Measuring time for program ' + p
			helper = get_language_helper(p, self.base_dir)
			helper.tests = self.get_tests_for_program(p)
			helper.verbose = self.verbose
			ok = helper.measure_time()

	def test_mutants(self):
		self.genereate_tests_if_needed()
		filtered_programs = [p for p in self.programs if self.programs_filter(p)]

		for mutator in self.mutators:
			for p in filtered_programs:
				print 'Testing mutants for program ' + p
				mutants = mutator.get_all_mutants(p)
				helper = get_language_helper(p, self.base_dir)
				helper.tests = self.get_tests_for_program(p)
				if self.logging: helper.clean_logs()
				helper.latex_output = self.latex_output
				helper.test_mutants(mutants)
				if self.logging:
					logs_dir = "logs/" + p + "/" + mutator.get_short_name()
					mkdir_cmd = TimerTask('mkdir -p ' + logs_dir)
					proc = mkdir_cmd.run()
					proc.wait()
					helper.move_logs(logs_dir)

	def genereate_tests_if_needed(self):
		try:
			a = self.tests
		except AttributeError:
			self.generate_tests()
