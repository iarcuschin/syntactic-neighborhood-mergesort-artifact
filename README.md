# Syntactic Neighborhood Mergesort Artifact

## Dependencies

The experiment runner uses Python 2 and Numpy. To install the required dependencies run the following commands:

> sudo apt install python virtualenv

> cd tester

> virtualenv .env

> source .env/bin/activate

> pip install numpy

In order to test the different versions of a problem, you might need:

- For haskell programs: GHC, which you can install by running 

> sudo apt install ghc

- For C programs: GCC, which you can install by running

> sudo apt install gcc

- For Java programs: java compiler, which you can install by running

> sudo apt install openjdk-8-jdk

## How to use

> cd tester

> source .env/bin/activate

> python main.py <problem> [<experiment>]

The problem can be 'all' or a specific problem.
The experiment can be 'time' or a mutator. If using 'time' you also have to provide an extra argument with the size of the test's suite.

Available problems: 	['md5', 'quicksort', 'crc-32', 'lzw', 'lcs', 'mergesort']

Available mutators: 	['fake', 'regex', 'char', 'qwerty']
